import { Component, OnInit } from '@angular/core';
import { TableData } from '../common/domain/tableData.interface';
import axios from 'axios';

@Component({
  selector: 'app-comprobantes',
  templateUrl: './comprobantes.component.html',
  styleUrls: ['./comprobantes.component.css']
})
export class ComprobantesComponent implements OnInit {
  stateModal = false;

  comprobantes: TableData | {} = {};
  comprobantesData: Array<any> = [];

  handleModalClose() {
    console.log('Modal oculto');
    this.stateModal = false;
  }

  showModal() {
    this.stateModal = true;
  }

  async getComprobantes() {
    const comprobantesData = await axios.get('http://localhost:3000/comprobantes', {
      params: {
        page: 1,
        limite: 2
      }
    });
    this.comprobantes = this.formatData(comprobantesData.data);
  }

  formatData(data: any): TableData {
    console.log(data)
    return {
      fields: [
        { header: 'FECHA', key: 'fecha', type: 'text' },
        { header: 'REMITENTE', key: 'idRemitente', type: 'text' },
        { header: 'DESTINATARIO', key: 'idDestinatario', type: 'text' },
        { header: 'TIPO CAMBIO', key: 'tipoCambio', type: 'text' },
        { header: 'MONTO MN', key: 'montoMN', type: 'text' },
        { header: 'MONTO ME', key: 'montoME', type: 'text' },
        { header: 'ACTIONS', key: 'id', type: 'buttons' },
      ],
      data: data
    };
  }

  ngOnInit(): void {
    this.getComprobantes();
  }
}
