
export interface TableData {
    fields: Array<{
        header: string,
        key: string,
        type?: 'text' | 'number' | 'buttons'
    }>
    data: Array<DataItems>
}

export interface DataItems {
    [key: string]: string | number | Array<DataItems>;
}