import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ClientesComponent } from './clientes/clientes.component';
import { ComprobantesComponent } from './comprobantes/comprobantes.component';
import { HomeComponent } from './home/home.component';
import { DropdownSelectComponent } from './components/dropdown-select/dropdown-select.component';
import { TopbarComponent } from './components/topbar/topbar.component';
import { TableComponent } from './components/table/table.component';
import { ModalComponent } from './components/modal/modal.component';

@NgModule({
  declarations: [
    AppComponent,
    ClientesComponent,
    ComprobantesComponent,
    HomeComponent,
    DropdownSelectComponent,
    TopbarComponent,
    TableComponent,
    ModalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
