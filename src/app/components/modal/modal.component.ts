import { Component, EventEmitter, HostListener, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {
  @Output() hideModal = new EventEmitter<any>();

  exitModal() {
    this.hideModal.emit(this.hideModal);
  }


  ngOnInit(): void {

  }
}
