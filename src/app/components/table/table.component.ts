import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit, OnChanges {
  @Input() payload: any = {};
  headers: Array<string> = [];
  fields: Array<string> = [];
  data: Array<any> = [];

  ngOnInit(): void {
    this.updateData();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['payload']) {
      this.updateData();
    }
  }

  updateData() {
    if (this.payload.fields) {
      this.headers = this.payload.fields.map((item: any) => item.header);
      this.fields = this.payload.fields.map((item: any) => item.key);
      this.data = this.payload.data;
    }
  }
}
