import { Component, HostListener } from '@angular/core';

@Component({
  selector: 'app-dropdown-select',
  templateUrl: './dropdown-select.component.html',
  styleUrls: ['./dropdown-select.component.css']
})
export class DropdownSelectComponent {
  showState = false
  selected = 'HOME'
  ngOnInit() { }

  toggle() {
    this.showState = !this.showState;
  }

  @HostListener('document:click', ['$event'])
  onClick(event: MouseEvent) {
    const dropdownElement = document.querySelector('.dropdown');
    if (!dropdownElement?.contains(event.target as Node)) {
      this.showState = false;
    }
  }
}
